
<?php

session_start();

if(empty($_SESSION["usuario"])){
    header("Location: index.php");
}
?>

<html>
<head>
  
  <title>Registrar</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
          
  <style>

    .container{
      display: flex;
        justify-content: center;
          align-items: center;
      margin:auto;
          width:50%;
      padding:50px;
          border: 3px solid black;
    } 
  
    .colums{
      background: white;
          margin: auto;
          padding: 1% 0;
  
    }
  
    .form-label{ 
      color: black;
      padding: auto;
    }
  
    #input{
      background: white;
      padding: auto;
    }
    .btn{
      background: #1298C6;
    }

    .error{
        display: flex;
        justify-content: center;
          align-items: center;
    }
  
    /*Reglas para responsive web*/
      @media screen and (min-width: 50%) {
          .container{width:100%;}
      }
    </style>

  </head>

<body class="bg-light">
  
  <header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">

        <ul class="navbar-nav mr-auto">
          <a class="navbar-brand mr-auto mr-lg-0" href="info.php">Home</a>
          <a class="nav-link" href= "registrar.php">Registrar alumnos</a>
          <a class="nav-link" href="login.php" id="boton-obtener">Cerrar sesion</a>
        </ul>
      </div>
    </nav>
  </header>


  <div class="container">
    <div class="columns" >
			
			<form action="registrarF.php?" method="POST">
			
				<h4 id = "titulo">Registrar Alumno</h4></br>
			
        <label class="form-label" for="input-numCuenta">Numero de cuenta:</label></br> 
				<input name="numCuenta" class="form-input" type="text" id="input-numCuenta" placeholder="Numero de Cuenta" required></input></br> 

				<label class="form-label" for="input-nombre">Nombre</label>
				<input name="nombre" class="form-input" type="text" id="input-nombre" placeholder="Nombre" required></br> 
        
				<label class="form-label" for="input-apePaterno">Apellido Paterno</label>
				<input name="apePaterno" class="form-input" type="text" id="input-apePaterno" placeholder="Apellido Paterno" required></input></br> 
				
				<label class="form-label" for="input-apePaterno">Apellido Materno</label>
				<input name="apeMaterno" class="form-input" type="text" id="input-apeMaterno" placeholder="Apellido Materno" required></input></br> 
		
				<label for="genero">Género</label>
				
        <select name="genero" id="genero"  required>
				
          <option value="H">Hombre</option>
					<option value="M">Mujer</option>
					<option value="O">Otro</option>
				
        </select><br/>

        <label class="form-label" for="input-apePaterno">Fecha Nacimiento</label>
				<input name="fecha_nac" class="form-input" type="date" id="input-fecha_nac"required></input></br> 

			
				<br/>
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña"></br>
				
        <p class ="error"><button type='submit' class="btn" color:"black">Registrar</button> </p>
				
			</form>
  
    </div>
  
  </div>
	
</body>
</html>
