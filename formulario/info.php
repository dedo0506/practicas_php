<?php 

session_start();

if(empty($_SESSION["usuario"])){
    header("Location: index.php");
}
?>

<html>
<head>
  
  <title>Informacion</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
          
  <style>

    .container{
      display: flex;
        justify-content: center;
          align-items: center;
      margin:auto;
          width:50%;
      padding:50px;
          border: 3px solid black;
    } 
  
    .colums{
      background: white;
          margin: auto;
          padding: 1% 0;
  
    }
  
    .form-label{ 
      color: black;
      padding: auto;
    }
  
    #input{
      background: white;
      padding: auto;
    }
    .btn{
      background: #1298C6;
    }

    .error{
        display: flex;
        justify-content: center;
          align-items: center;
    }
  
    /*Reglas para responsive web*/
      @media screen and (min-width: 50%) {
          .container{width:100%;}
      }
    </style>

  </head>

<body class="bg-light">
  
  <header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">

        <ul class="navbar-nav mr-auto">
          <a class="navbar-brand mr-auto mr-lg-0" href="info.php">Home</a>
          <a class="nav-link" href= "registrar.php">Registrar alumnos</a>
          <a class="nav-link" href="login.php" id="boton-obtener">Cerrar sesion</a>
        </ul>
      </div>
    </nav>
  </header>

  <main>
	
    <section>
    <div>
    <br/>
    </div>
    
    <div id="info_usuario">
    <p><h3 class="titulo_info">Usuario Autenticado</h3></p>
            <div>
                <p>
                Nombre: 
                <?php echo $_SESSION["alumno"][$_SESSION["usuario"]]["nombre"] . " " . $_SESSION["alumno"][$_SESSION["usuario"]]["apePaterno"]; ?>
                </p>
                
                <p>
                Número de cuenta: 
                <?php echo $_SESSION["usuario"]; ?>
                </p>
                <p>
                Fecha de nacimiento: 
                <?php echo $_SESSION["alumno"][$_SESSION["usuario"]]["fecha_nac"]; ?>
                </p>
            </div>
    </table> 
    <div>
    
    <br/>
    
    <div id="usuarios">
    <p><h3 class="titulo_info">Usuarios registrados</h3></p>
    <table>
        <thead>
          <th scope="col">Número de cuenta </th>
          <th scope="col">Nombre completo</th>
          <th scope="col">Fecha de nacimiento</th>
        </thead>
        <tbody>
          <?php
          foreach ($_SESSION["alumno"] as $key => $value) {
            echo "<tr>";
            echo "<td>" . $value["numCuenta"] . "</td>";
            echo "<td>" . $value["nombre"] . " " . $value["apePaterno"] . " " . $value["apeMaterno"] . "</td>";
            echo "<td>" . $value["fecha_nac"] . "</td>";
            echo "</tr>";
          }
          ?>
        </tbody>
    </table> 
    </div>
    
    </section>
    
</main>

</body>
</html>
