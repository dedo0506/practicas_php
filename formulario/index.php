<html>
<head>
  
  <title>Formularios</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
          
  <style>

    .container{
      display: flex;
        justify-content: center;
          align-items: center;
      margin:auto;
          width:50%;
      padding:50px;
          border: 3px solid black;
    } 
  
    .colums{
      background: white;
          margin: auto;
          padding: 1% 0;
  
    }
  
    .form-label{ 
      color: black;
      padding: auto;
    }
  
    #input{
      background: white;
      padding: auto;
    }
    .btn{
      background: #1298C6;
    }

    .error{
        display: flex;
        justify-content: center;
          align-items: center;
    }
  
    /*Reglas para responsive web*/
      @media screen and (min-width: 50%) {
          .container{width:100%;}
      }
    </style>

  </head>

  <body class="bg-light">
  
  <header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
      
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">

        <ul class="navbar-nav mr-auto">
          <a class="navbar-brand mr-auto mr-lg-0" href="info.php">Home</a>
          <a class="nav-link" href= "registrar.php">Registrar alumnos</a>
          <a class="nav-link" href="cerrarSesion.php" id="boton-obtener">Cerrar sesion</a>
        </ul>
      </div>
    </nav>
  </header>


    <div class="container">
    <div class="columns" >
			
			<form action="login.php?" method="POST">
			
				<h4 id = "titulo">LOGIN</h4></br>

				<label class="form-label" for="input-numCuenta">Numero de cuenta:</label></br> 
				<input name="numCuenta" class="form-input" type="text" id="numCuenta" placeholder="Numero de Cuenta"/></br></br> 

				<label class="form-label" for="input-password">Contraseña</label></br> 
				<input name="password" class="form-input" type="password" id="pass" placeholder="Contraseña"/>
        </br></br> 

          <p class= "error">

              <!--En caso de error-->
              <?php

              if (isset($_GET["error"])) {
                  
                  $error = $_GET["error"];
                          
                  if ($error == "invalido") {
                      
                      echo "<br><h4>El usuario y/o la contraseña no son válidos..</h4><br>";
                  
                  }
                  
                  if ($error == "empty") {
                      
                      echo "<br><h4>Llene todos los campos.</h4><br>";
                  
                  }
              }

              ?>
          </p>

				<button type="submit" class="btn" >Entrar</button>
				
			</form>
	
    </div>
  
  </div>

</body>

</html>