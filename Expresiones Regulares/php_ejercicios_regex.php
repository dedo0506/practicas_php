<html> 
<head> 
    <title>Expresiones regulares</title> 
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
          
  <style>

    .container{
      margin:auto;
      width:50%;
      padding:50px;
      border: 3px solid black;
    } 
    
    /*Reglas para responsive web*/
      @media screen and (min-width: 50%) {
          .container{width:100%;}
      }
    </style>
</head> 
<body> 
    <div class="container"> 
        <h4>Emails correctos</h4>
        <?php
        //AUTOR: DAVILA OLIVARES DANIELA ELIZABETH
        //Realizar una expresión regular que detecte emails correctos.

            function validar_email($str)
            {
            $matches = null;
            return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
            }


            // probando con un formato valido
            $email = "dedo.0506@outlook.com.es";
            if (validar_email($email) == 1){

                echo "<br>El formato del email <b>$email</b> es valido <br>";

            }else {

                echo "<br>El formato del email <b>$email</b> no valido <br>";

            }

            //probando con un formato no valido
            $emailNoValido = "hola&correo@ejemplo.com";
            if (validar_email($emailNoValido) == 1){

                echo "<br>El formato del email <b>$emailNoValido</b> es valido <br>";

            }else {

                echo "<br>El formato del email <b>$emailNoValido</b> no valido <br>";

            }
        ?>
    </div>    

    <div class="container">
        <h4>Curps correctos</h4>
        <?php
            //Realizar una expresion regular que detecte Curps Correctos    //ABCD123456EFGHIJ78
            function validar_curp($str)
            {
            $matches = null;
            return (1 === preg_match('/(^[A-Z]{4})([0-9]{6})([A-Z]{6})([0-9]{2})$/', $str, $matches));
            }

            // probando con un formato valido
            $curp = "ABCD123456EFGHIJ78";
            if (validar_curp($curp) == 1){

                echo "<br>El formato de curp <b>$curp</b> es valido <br>";

            }else {

                echo "<br>El formato de curp <b>$curp</b> no valido <br>";

            }

            //probando con un formato no valido
            $curpNoValido = "ABC01234567FGHIJ78";
            if (validar_curp($emailNoValido) == 1){

                echo "<br>El formato de curp <b>$curpNoValido</b> es valido <br>";

            }else {

                echo "<br>El formato de curp <b>$curpNoValido</b> no valido <br>";

            }
        ?>
    </div>

    <div class="container">
        <h4>Validar palabras con longitud mayor a 50</h4>
        <?php
        //Realizar una expresion regular que detecte palabras de longitud mayor a 50
        //formadas solo por letras.

        function validar_frase($str)
        {
        
            return (preg_match('/^([A-z]{50})/', $str));
        }

        // probando con un formato valido
        $frase = "Esternocleidooccipitomastoideo Esternocleidomastoideísticamente";
        if (validar_frase($frase) == 0){

            echo "<br>En <b>$frase</b> se encontraron palabras con mas de 50 letras <br>";

        }else {

            echo "<br>En <b>$frase</b>  no se encontraron palabras con mas de 50 letras <br>";

        }

        //probando con un formato no valido
        $fraseMal = "hola mundo";
        if (validar_frase($fraseMal) == 1){

            echo "<br>En <b>$fraseMal</b> se encontraron palabras con mas de 50 letras <br>";

        }else {

            echo "<br>En <b>$fraseMal</b>  no se encontraron palabras con mas de 50 letras <br>";

        }
        ?>
    
    </div>

    <div class="container">
        <h4>Escapar Simbolos especiales</h4>
        <?php

        //Crea una funcion para escapar los simbolos especiales.

        $cadena = "<br>Esta\ es una. cadena con símbolos especiales &#$+?.<br>";

        echo "$cadena";

        function escapar(&$string) {
            $string = preg_quote($string);
        }

        escapar($cadena);

        echo "<br>$cadena <br>";

        ?>
    </div>

    <div class="container">
        <h4>Validar numero decimal</h4>
        <?php
        //Crear una expresion regular para detectar números decimales.
        function validar_numDec($str){
            $matches = null;
            return (1 === preg_match('/[0-9]+\.[0-9]+/', $str, $matches));
        }


        // probando con un formato valido
        $num = "100789";
        if (validar_numDec($num) == 0){

            echo "<br>El numero <b>$num</b> es decimal <br>";

        }else {

            echo "<br>El numero <b>$num</b> no es decimal <br>";

        }

        //probando con un formato no valido
        $numNDec = "100.789";
        if (validar_numDec($numNDec) == 0){

            echo "<br>El numero <b>$numNDec</b> es decimal <br>";

        }else {

            echo "<br>El numero <b>$numNDec</b> no es decimal <br>";

        }
        ?>

    </div>

	</body>
</html>     