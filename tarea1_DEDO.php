<html> 
	<head> 
		<title>Piramide de * </title> 
	</head> 
	<body> 
		<?php 

        /*
        Programa: Piramide de *
        Autor: Davila Olivares Daniela Elizabeth
        Fecha 07/11/2022
        */

        $count = 1;
		while($count <=30){
            
            $i = 1;

            echo "<div align='center'>";
            while($i < $count) {

                echo("*");

                $i++;
            }
            echo "</div><br>";

            $count++;
        }
		?>
	</body>
</html> 